resource "aws_iam_instance_profile" "ec2-instance-profile" {
  name  = "${var.deployment-prefix}-ec2-instance-profile"
  role  = "${aws_iam_role.ec2-role.name}"
}

resource "aws_iam_policy" "ec2-role-policy" {
  name        = "${var.deployment-prefix}-ec2-s3-policy"
  path        = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:GetObject",
        "s3:ListBucket",
        "s3:PutObject"
      ],
      "Resource": [
        "arn:aws:s3:::${var.s3-bucket-name}/*",
        "arn:aws:s3:::${var.s3-bucket-name}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_role" "ec2-role" {
  name               = "${var.deployment-prefix}-ec2-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "ec2-role-attachment" {
  name          = "${var.deployment-prefix}-ec2-role-attachment"
  roles         = ["${aws_iam_role.ec2-role.name}"]
  policy_arn    = "${aws_iam_policy.ec2-role-policy.arn}"
}