#!/bin/bash
sudo sed 's/requirepass foobared/requirepass ${redis-auth-token}/g' /etc/redis.conf | sudo tee /etc/redis.password.conf &&
sudo cp /etc/redis.conf /etc/redis.conf.bak &&
sudo mv /etc/redis.password.conf /etc/redis.conf &&
sudo systemctl restart redis
