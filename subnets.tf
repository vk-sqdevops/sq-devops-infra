#https://github.com/hashicorp/terraform/issues/58
locals {
  awsZones      = "${split(",", lookup(var.aws-azs, var.aws-region))}"
  azCount       = "${length(local.awsZones)}"
}

# Create public subnets in all availability zones
resource "aws_subnet" "public-subnet" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  count                   = "${local.azCount}"
  cidr_block              = "${cidrsubnet(aws_vpc.vpc.cidr_block, 2, count.index)}"
  availability_zone       = "${element(local.awsZones, count.index)}"
  map_public_ip_on_launch = true
  
  tags {
    Name = "${var.deployment-prefix}-sn-public-${element(local.awsZones, count.index)}"
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"    
  }
}

# Create private subnets in all availability zones
resource "aws_subnet" "private-subnet" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  count                   = "${local.azCount}"
  cidr_block              = "${cidrsubnet(aws_vpc.vpc.cidr_block, 2, (count.index + local.azCount))}"
  availability_zone       = "${element(local.awsZones, count.index)}"
  map_public_ip_on_launch = false
  
  tags {
    Name = "${var.deployment-prefix}-sn-private-${element(local.awsZones, count.index)}"
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"    
  }
}
