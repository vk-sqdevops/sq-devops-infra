# Creds
variable "aws-access-key" {}
variable "aws-secret-key" {}

# Region
variable "aws-region" {}

# Subnets
variable "aws-azs" {
  type = "map"
}

# Naming and tagging
variable "deployment-prefix" {}
variable "organization" {}
variable "project" {}
variable "environment" {}

# IP Addresses to suffice for 4 subnets, ~254 addresses each
variable "vpc-cidr-block" {}

# Redis Cache
variable "redis-server-ami-id" {}
variable "redis-server-instance-type" {}
variable "redis-server-key-pair" {}
variable "redis-cache-instance-type" {}
variable "redis-cache-clusters-count" {}
variable "redis-cache-instance-count" {}
variable "redis-cache-parameter-group-name" {}
variable "redis-cache-port" {}

# s3-bucket-name
variable "s3-bucket-name" {}
