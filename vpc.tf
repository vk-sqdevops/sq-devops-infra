resource "aws_vpc" "vpc" {
  cidr_block              = "${var.vpc-cidr-block}" 
  enable_dns_hostnames="true"
  tags {
    Name = "${var.deployment-prefix}-vpc"
    Organization = "${var.organization}"
    Project = "${var.project}"
    Environment = "${var.environment}"
  } 
}

resource "aws_default_route_table" "vpc_default_rt" {
  default_route_table_id = "${aws_vpc.vpc.default_route_table_id}"
  route {
    cidr_block = "0.0.0.0/0"   
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags {
    Name                = "${var.deployment-prefix}-public-route-table"
    Organization        = "${var.organization}"
    Project             = "${var.project}"
    Environment         = "${var.environment}"
  } 
}
